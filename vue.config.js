module.exports = {
  runtimeCompiler: true,
  configureWebpack: {
    context: __dirname,
    node: {
      __filename: true
    },
  },
  css: {
    loaderOptions: {
      less: {
        lessOptions: {
          // If you are using less-loader@5 please spread the lessOptions to options directly
          modifyVars: {
            'font-family': "'Manrope', sans-serif;",
            'font-size-base': '14px',
            'primary-color': '#1890FF',
            'label-color': '#868686',
            'input-height-lg': '42px',
            'input-border-color': '#BDBDBD',
            'input-color': '#696969',
            'btn-height-lg': '42px',
            // 'border-radius-base': '14px',
          },
          javascriptEnabled: true,
        },
      },
    },
  },
  chainWebpack: config => {
    const svgRule = config.module.rule('svg');
    svgRule.uses.clear();
    svgRule.use('vue-svg-loader').loader('vue-svg-loader');
  },
  pwa: {
    name: 'My App',
    themeColor: '#4DBA87',
    msTileColor: '#000000',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      swSrc: './src/service-worker.js',
    }
  }
};
