export default {
  DATE_TIME_FORMAT: 'MM/DD/YYYY hh:mmA',
  DATE_FORMAT: 'MM/DD/YYYY',
  EVENT: {
    ON_SCROLL_BOTTOM: 'ON_SCROLL_BOTTOM',
    ON_BACK_FROM_QUIZ: 'ON_BACK_FROM_QUIZ'
  },
  MENUS: [
    {
      url: '/dashboard',
      key: 'dashboard',
      name: 'Dashboard',
      title: 'Điều Khiển Thiết Bị',
      icon: 'MyIconDashboard',
    },
    {
      url: '/detail/temperature',
      key: 'temperature',
      name: 'Temperature',
      title: 'Nhiệt Độ',
      icon: 'MyIconTemperature',
    },
    {
      url: '/detail/moisture',
      key: 'moisture',
      name: 'Moisture',
      title: 'Độ Ẩm Không Khí',
      icon: 'MyIconMoisture',
    },
    {
      url: '/detail/soilmoisture',
      key: 'soilmoisture',
      name: 'SoilMoisture',
      title: 'Độ Ẩm Đất',
      icon: 'MyIconSoilMoisture',
    },
    {
      url: '/detail/light',
      key: 'light',
      name: 'Light',
      title: 'Cường Độ Sáng',
      icon: 'MyIconSun',
    },
    {
      url: '/detail/roof',
      key: 'roof',
      name: 'Roof',
      title: 'Mái che',
      icon: 'MyIconRoofing',
    },
  ],
  FOOTER_MENUS: [
    {
      url: '/explore',
      key: 'explore',
      name: 'Explore',
      title: 'About us',
    },
    {
      url: '/library',
      key: 'privacy-policy',
      name: 'Library',
      title: 'Privacy policy',
    },
  ],
};
