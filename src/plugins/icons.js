// plugin.js
import Dashboard from '@/assets/svgs/dashboard.svg';
import Menu from '@/assets/svgs/menu.svg';
import Moisture from '@/assets/svgs/moisture.svg';
import Roofing from '@/assets/svgs/roofing.svg';
import SoilMoisture from '@/assets/svgs/soilmoisture.svg';
import Sun from '@/assets/svgs/sun.svg';
import Temperature from '@/assets/svgs/temperature.svg';

// This exports the plugin object.
export default {
  // The install method will be called with the Vue constructor as         
  // the first argument, along with possible options
  install (Vue) {
    // Add or modify global methods or properties.
    // Add a component or directive to your plugin, so it will be installed globally to your project.
    Vue.component('MyIconDashboard', Dashboard);
    Vue.component('MyIconMenu', Menu);
    Vue.component('MyIconMoisture', Moisture);
    Vue.component('MyIconRoofing', Roofing);
    Vue.component('MyIconSoilMoisture', SoilMoisture);
    Vue.component('MyIconSun', Sun);
    Vue.component('MyIconTemperature', Temperature);
  }
}
