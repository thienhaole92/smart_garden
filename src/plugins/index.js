import Vue from 'vue';

// Vue constants
import Constants from './constants';
import Components from './components';
import Icons from './icons';
import Event from './event';

Vue.use(Constants);

// Vue global components
Vue.use(Components);

// Vue custom svg icons
Vue.use(Icons);

//
Vue.use(Event);
