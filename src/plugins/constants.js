import constants from '../constants';

// This exports the plugin object.
export default {
  // The install method will be called with the Vue constructor as
  // the first argument, along with possible options
  install (Vue) {
    // Add Vue instance methods by attaching them to Vue.prototype.
    Vue.prototype.$constants = constants;
  }
}
