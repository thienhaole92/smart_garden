import Vue from 'vue';
import VueRouter from 'vue-router';
import Main from '../views/Main';
import Detail from "../views/Detail";

import Dashboard from '../components/page/Dashboard/Dashboard';
import Temperature from "../components/page/Temperature/Temperature";
import Moisture from "../components/page/Moisture/Moisture";
import SoilMoisture from "../components/page/SoilMoisture/SoilMoisture";
import Light from "../components/page/Light/Light";
import Roof from "../components/page/Roof/Roof";

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Main',
    component: Main,
    redirect: '/dashboard',
    children: [
      {
        path: '/dashboard',
        name: 'Dashboard',
        component: Dashboard
      },
    ]
  },
  {
    path: '/detail',
    name: 'Detail',
    component: Detail,
    children: [
      {
        path: '/detail/temperature',
        name: 'Temperature',
        component: Temperature
      },
      {
        path: '/detail/moisture',
        name: 'Moisture',
        component: Moisture
      },
      {
        path: '/detail/soilmoisture',
        name: 'SoilMoisture',
        component: SoilMoisture
      },
      {
        path: '/detail/light',
        name: 'Light',
        component: Light
      },
      {
        path: '/detail/roof',
        name: 'Roof',
        component: Roof,
      },
    ]
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router
