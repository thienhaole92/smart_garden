import constants from './constants';

const setMobile = (state, isMobile) => {
  state.isMobile = isMobile;
};

const showLoading = (state) => {
  state.loading = true;
};

const hideLoading = (state) => {
  state.loading = false;
};

const toggleMenu = (state, collapsed) => {
  state.collapsed = !collapsed;
};

const hideMenu = (state) => {
  state.collapsed = false;
};

export default {
  [constants.MUTATION_SET_MOBILE]: setMobile,
  [constants.MUTATION_SHOW_LOADING]: showLoading,
  [constants.MUTATION_HIDE_LOADING]: hideLoading,
  [constants.MUTATION_TOGGLE_MENU]: toggleMenu,
  [constants.MUTATION_HIDE_MENU]: hideMenu,
};
