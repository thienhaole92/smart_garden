import constants from './constants';

function setMobile({commit}, isMobile) {
  return commit(constants.MUTATION_SET_MOBILE, isMobile);
}

function showLoading({commit}) {
  return commit(constants.MUTATION_SHOW_LOADING);
}

function hideLoading({commit}) {
  return commit(constants.MUTATION_HIDE_LOADING);
}

function toggleMenu({commit, state}) {
  return commit(constants.MUTATION_TOGGLE_MENU, state.collapsed);
}

function hideMenu({commit}) {
  return commit(constants.MUTATION_HIDE_MENU, false);
}


export default {
  [constants.ACTION_SET_MOBILE]: setMobile,
  [constants.ACTION_SHOW_LOADING]: showLoading,
  [constants.ACTION_HIDE_LOADING]: hideLoading,
  [constants.ACTION_TOGGLE_MENU]: toggleMenu,
  [constants.ACTION_HIDE_MENU]: hideMenu,
};
