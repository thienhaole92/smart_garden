import constants from './constants';

const isMobile = (state) => {
  return state.isMobile;
};

const loading = (state) => {
  return state.loading;
};

const collapsed = (state) => {
  return state.collapsed;
};

export default {
  [constants.GETTER_IS_MOBILE]: isMobile,
  [constants.GETTER_LOADING]: loading,
  [constants.GETTER_COLLAPSED]: collapsed,
};
