import constants from './constants';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';
import defaultState from './defaultState';
const state = defaultState;

export default {
  namespaced: true,
  namespace: __filename,
  constants,
  actions,
  getters,
  mutations,
  state
};
