const defaultState = {
  loading: false,
  isMobile: false,
  collapsed: false
};

export default defaultState;
