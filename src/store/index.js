import Vue from 'vue';
import Vuex from 'vuex';
import _ from 'lodash';
import layout from './layout';

Vue.use(Vuex);

export const initialStoreModules = {
  [layout.namespace]: layout,
};

const store = new Vuex.Store({
  modules: _.cloneDeep(initialStoreModules)
});

export {
  store
};
