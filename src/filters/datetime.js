import Vue from 'vue';
import moment from 'moment-timezone';
import constants from '../constants';

Vue.filter('datetime', function (value) {
  if (!value) return '';
  const datetime = moment(value).locale('en').format(constants.DATE_TIME_FORMAT);
  return datetime.charAt(0).toUpperCase() + datetime.slice(1);
});
