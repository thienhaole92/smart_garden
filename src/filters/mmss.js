import Vue from 'vue';

Vue.filter('mmss', function (value) {
  if (!value || value == Number.POSITIVE_INFINITY || value == Number.NEGATIVE_INFINITY) return '00:00';
  const minutes = Math.floor(value / 60);
  let seconds = value % 60;

  if (seconds < 10) {
    seconds = `0${seconds}`;
  }
  return `${minutes}:${seconds}`;
});
