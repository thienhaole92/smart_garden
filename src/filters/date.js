import Vue from 'vue';
import moment from 'moment-timezone';
import constants from '../constants';

Vue.filter('date', function (value) {
  if (!value) return '';
  const date = moment(value).locale('en').format(constants.DATE_FORMAT);
  return date.charAt(0).toUpperCase() + date.slice(1);
});
