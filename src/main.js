import Vue from 'vue';
import App from './App.vue';
import router from './router';
import { store } from './store';
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.less';
import VueMoment from 'vue-moment';
import moment from 'moment-timezone';
import TopProgress from 'vue-top-progress'
import CircularCountDownTimer from 'vue-circular-count-down-timer';
import lineClamp from 'vue-line-clamp';

// Plugins
import './plugins';

// Register filters
import './filters';

Vue.config.productionTip = false;

// Ant design vue
Vue.use(Antd);

// Vue top progress
Vue.use(TopProgress);

// Vue moment
Vue.use(VueMoment, {
  moment,
});

Vue.use(lineClamp);

// Vue auth
Vue.router = router; // Required for @websanova/vue-auth to works
Vue.use(CircularCountDownTimer);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
